# Natural Language Processing in R #

In this project we apply our knowledge on NLP to sentiment analysis

### Task Description ###

* [SemEval Website] (http://alt.qcri.org/semeval2016/task4/) 
* Subtask A: Message Polarity Classification
* Subtask B: Tweet classification according to a two-point scale
* Subtask C: Tweet classification according to a five-point scale
* Subtask D: Tweet quantification according to a two-point scale
* Subtask E: Tweet quantification according to a five-point scale

### Feature Enginnering and Approach ###

* Cleaning the data
	* Remove Not available tweets
	* Convert to lower case
	* Replace contractions and abbreviations
	* Use twitter dictionary to convert common slangs to correct words
	* Replace emoticons with respective sentiment ( happy, crying, sad, ...)

* N-Gram score
	* In most approaches we used bigram score calcuation
	* Find the common negation words like No, Not, Never, without, ..
	* If there is a positive word after these words add a bigram score of negative value

* Stemming and POS tagging
	* We decided to use [TreeTagger] (http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/) to do POS tagging and Lemmatization

* Bing Sentiment
	* Using the Bing Sentiment lexicon we find the appropriate sentiment on the lemma
	* We also keep the POS tags such as adjective, adverb, noun and determiners for the model building

### Model Building ###

We used Naive Bayes / SVM to get the results and achieved accuraacy of above baseline in all the tasks.

### Reach out to us for more details ###

* Srinivasrk92@gmail.com
* kashyapadithya64@gmail.com
* ash22393@gmail.com