

#################### LOAD LIBRARIES #######################

load_libraries <- function(){
  # URL parsing
  library("koRpus")
  library ("devtools")
  #install_github("trinker/qdapRegex")
  library("qdapRegex")
  library("hunspell")
  library("tidytext")
  library("dplyr")
  library("ggplot2")
  library("tm")
  library("qdap")
  library("dplyr")
  library("stringr")
  library("tm")
  library("qdap")
  library("tidyr")
  library("wordcloud")
  library("e1071")
  library("janeaustenr")
}

######################## END ##########################################


#################### Load DATA ################################

load_data <- function() {
  
  df = read.table(file = '../../NLP python - R/old-data/twitter-2013train-A.txt', sep = '\t', quote = "", stringsAsFactors = FALSE, header = FALSE, encoding = "utf-8", comment.char = "")
  df = df[,2:3]
  
  colnames(df) <- c("sentiment", "tweet")
  
  df_1 <- read.table(file = '../../NLP python - R/old-data/twitter-2015train-A.txt', sep = '\t', quote = "", stringsAsFactors = FALSE, header = FALSE, encoding = "utf-8", comment.char = "")
  df_1 = df_1[,2:3]
  
  colnames(df_1) <- c("sentiment", "tweet")
  
  df <- rbind(df, df_1)
  
  
  df_1 <- read.table(file = '../../NLP python - R/old-data/twitter-2013dev-A.txt', sep = '\t', quote = "", stringsAsFactors = FALSE, header = FALSE, encoding = "utf-8", comment.char = "")
  df_1 = df_1[,2:3]
  colnames(df_1) <- c("sentiment", "tweet")
  df <- rbind(df, df_1)
  
  
  df_1 <- read.table(file = '../../NLP python - R/old-data/twitter-2016dev-A.txt', sep = '\t', quote = "", stringsAsFactors = FALSE, header = FALSE, encoding = "utf-8", comment.char = "")
  df_1 = df_1[,2:3]
  colnames(df_1) <- c("sentiment", "tweet")
  df <- rbind(df, df_1)
  
  
  df_1 <- read.table(file = '../../NLP python - R/old-data/twitter-2016devtest-A.txt', sep = '\t', quote = "", stringsAsFactors = FALSE, header = FALSE, encoding = "utf-8", comment.char = "")
  df_1 = df_1[,2:3]
  colnames(df_1) <- c("sentiment", "tweet")
  df <- rbind(df, df_1)
  
  colnames(df_1) <- c("sentiment", "tweet")
  
  df_1 <- read.table(file = '../../NLP python - R/old-data/twitter-2016train-A.txt', sep = '\t', quote = "", stringsAsFactors = FALSE, header = FALSE, encoding = "utf-8", comment.char = "")
  df_1 = df_1[,2:3]
  colnames(df_1) <- c("sentiment", "tweet")
  df <- rbind(df, df_1)
  
  
  rm(df_1)
  return(df)
}

###################### END ###############################

#################### CLEAN DATA ##########################

clean_data <- function(df) {
  df$tweet <- tolower(df$tweet)
  df$tweet <- gsub("u002c", " ,", df$tweet)
  df$tweet <- gsub("u2018", "'", df$tweet)
  df$tweet <- gsub("u2019", "'", df$tweet)
  df$tweet <- (gsub("\\s+", " ", df$tweet))
  
  df$tweet <- gsub("\\bab\\b", "about", df$tweet)
  df$tweet <- gsub("\\babt\\b", "about", df$tweet)
  df$tweet <- gsub("\\bdidn\\b", "didn\'t", df$tweet)
  df$tweet <- gsub("\\bhavent\\b", "haven\'t", df$tweet)
  df$tweet <- gsub("\\bcouldnt\\b", "couldn\'t", df$tweet)
  df$tweet <- gsub("\\bdidnt\\b", "didn\'t", df$tweet)
  df$tweet <- gsub("\\bisnt\\b", "isn\'t", df$tweet)
  df$tweet <- gsub("\\bcant\\b", "can\'t", df$tweet)
  df$tweet <- gsub("\\bshouldnt\\b", "shouldn\'t", df$tweet)
  df$tweet <- gsub("\\bwouldnt\\b", "wouldn\'t", df$tweet)
  df$tweet <- gsub("\\byall\\b", "you all", df$tweet)
  df$tweet <- gsub("\\bwasnt\\b", "wasn\'t", df$tweet)
  df$tweet <- gsub("\\bdoesnt\\b", "doesn\'t", df$tweet)
  df$tweet <- gsub("\\bdont\\b", "don\'t", df$tweet)
  df$tweet <- gsub("\\bhadnt\\b", "hadn\'t", df$tweet)
  df$tweet <- gsub("\\bhasnt\\b", "hasn\'t", df$tweet)
  df$tweet <- gsub("\\bcouldve\\b", "could\'ve", df$tweet)
  df$tweet <- gsub("\\bmightve\\b", "might\'ve", df$tweet)
  df$tweet <- gsub("\\bitll\\b", "it will", df$tweet)
  df$tweet <- gsub("\\bits\\b", "it\'s", df$tweet)
  df$tweet <- gsub("\\bthats\\b", "that\'s", df$tweet)
  df$tweet <- gsub("\\bwheres\\b", "where\'s", df$tweet)
  df$tweet <- gsub("\\bive\\b", "i\'ve", df$tweet)
  df$tweet <- gsub("\\bill\\b", "i'll", df$tweet)
  df$tweet <- gsub("\\bab\\b", "about", df$tweet)
  df$tweet <- gsub("\\babt\\b", "about", df$tweet)
  df$tweet <- gsub("\\bawsum\\b", "awesome", df$tweet)
  df$tweet <- gsub("\\bb/c\\b", "because", df$tweet)
  df$tweet <- gsub("\\bbcoz\\b", "because", df$tweet)
  df$tweet <- gsub("\\bbc\\b", "because", df$tweet)
  df$tweet <- gsub("\\bb\\b", "be", df$tweet)
  df$tweet <- gsub("\\b4\\b", "for", df$tweet)
  df$tweet <- gsub("\\bb4\\b", "before", df$tweet)
  df$tweet <- gsub("\\beetweet\\b", "hot tweet", df$tweet)
  df$tweet <- gsub("\\bb4\\b", "before", df$tweet)
  df$tweet <- gsub("\\bbfn\\b", "bye for now", df$tweet)
  df$tweet <- gsub("\\bbgd\\b", "background", df$tweet)
  df$tweet <- gsub("\\bbr\\b", "best regards", df$tweet)
  df$tweet <- gsub("\\bbulltwit\\b", "false", df$tweet)
  df$tweet <- gsub("\\bchk\\b", "check", df$tweet)
  df$tweet <- gsub("\\bcld\\b", "could", df$tweet)
  df$tweet <- gsub("\\bclk\\b", "click", df$tweet)
  df$tweet <- gsub("\\bbulltwit\\b", "false", df$tweet)
  df$tweet <- gsub("\\bcranktweet\\b", "misleading", df$tweet)
  df$tweet <- gsub("\\bcre8\\b", "create", df$tweet)
  df$tweet <- gsub("\\bda\\b", "the", df$tweet)
  df$tweet <- gsub("\\bdrunktwittering\\b", "drunk", df$tweet)
  df$tweet <- gsub("\\begotwistical\\b", "ego", df$tweet)
  df$tweet <- gsub("\\bem\\b", "email", df$tweet)
  df$tweet <- gsub("\\bema\\b", "email address", df$tweet)
  df$tweet <- gsub("\\bfab\\b", "fabulous", df$tweet)
  df$tweet <- gsub("\\bfav\\b", "favorite", df$tweet)
  df$tweet <- gsub("\\bfomo\\b", "fear of missing out", df$tweet)
  df$tweet <- gsub("\\bftl\\b", "for the loss", df$tweet)
  df$tweet <- gsub("\\bfyi\\b", "for your information", df$tweet)
  df$tweet <- gsub("\\bfml\\b", "fuck my life", df$tweet)
  df$tweet <- gsub("\\bhand\\b", "have a nice day", df$tweet)
  df$tweet <- gsub("\\bH.O\\b", "hang over", df$tweet)
  df$tweet <- gsub("\\bic\\b", "i see", df$tweet)
  df$tweet <- gsub("\\bicymi\\b", "in case you missed me", df$tweet)
  df$tweet <- gsub("\\bidk\\b", "i don't know", df$tweet)
  df$tweet <- gsub("\\bkk\\b", "cool cool", df$tweet)
  df$tweet <- gsub("\\bitz\\b", "it is", df$tweet)
  df$tweet <- gsub("\\bic\\b", "i see", df$tweet)
  df$tweet <- gsub("\\boh\\b", "overhead", df$tweet)
   df$tweet <- gsub("\\bsmh\\b", "not agreeing", df$tweet)
  df$tweet <- gsub("\\btbh\\b", "to be honest", df$tweet)
  df$tweet <- gsub("\\btbt\\b", "throwback thrusday", df$tweet)
  df$tweet <- gsub("\\bic\\b", "i see", df$tweet)
  df$tweet <- gsub("\\btwabulous\\b", "fablous tweet", df$tweet)
  df$tweet <- gsub("\\btwbabe\\b", "dear", df$tweet)
  df$tweet <- gsub("\\btwis\\b", "this", df$tweet)
  df$tweet <- gsub("\\bTwitter-ific\\b", "terrific", df$tweet)
  df$tweet <- gsub("\\bU\\b", "you", df$tweet)
  df$tweet <- gsub("\\bTwitter-ific\\b", "terrific", df$tweet)
  df$tweet <- gsub("\\bwoz\\b", "was", df$tweet)
  df$tweet <- gsub("\\bwtv\\b", "whatever", df$tweet)
  df$tweet <- gsub("\\byolo\\b", "you only live once", df$tweet)
  df$tweet <- gsub("\\byoyo\\b", "you are on your own", df$tweet)
  df$tweet <- gsub("\\byay\\b", "happy", df$tweet)
  df$tweet <- gsub("\\bwcw\\b", "women crush wednesday", df$tweet)
  df$tweet <- gsub("\\bdam\b", "don't anger me", df$tweet)
  df$tweet <- gsub("\\bomg\\b", "oh my god", df$tweet)
  df$tweet <- gsub("\\blol\\b", "laugh out loud", df$tweet)
  df$tweet <- gsub("\\brofl\\b", "rolling on the floor and laughing", df$tweet)
  df$tweet <- gsub("\\bzomg\\b", "oh my god to the max", df$tweet)
  df$tweet <- gsub(':)', "happy", df$tweet)
  df$tweet <- gsub('\\:\\(', "sad", df$tweet)
  df$tweet <- gsub(':o)', "clown nose smiley face", df$tweet)
  df$tweet <- gsub('\\b:p\\b', "winking", df$tweet)
  df$tweet <- gsub("\\b:d\\b", "happy", df$tweet)
  df$tweet <- gsub("\\bpeeps\\b", "people", df$tweet)
  df$tweet <- gsub("\\bnd\\b", "and", df$tweet)
  df$tweet <- gsub("\\b&\\b", "and", df$tweet)
  df$tweet <- gsub("\\bomfg\\b", "oh my fucking god", df$tweet)
  df$tweet <- gsub("\\bawesoomeee\\b", "awesome", df$tweet)
  df$tweet <- gsub("\\bargh\\b", "anguish", df$tweet)
  df$tweet <- gsub("\\baargh\\b", "anguish", df$tweet)
  df$tweet <- gsub("\\bhuh\\b", "disbelief", df$tweet)
  df$tweet <- gsub("\\buh\\b", "disbelief", df$tweet)
  df$tweet <- gsub("\\banniv\\b", "anniversary", df$tweet)
  df$tweet <- gsub("\\bc\'mon\\b", "come on", df$tweet)
  df$tweet <- gsub("\\bdey\\b", "they", df$tweet)
  df$tweet <- gsub("\\bmon\\b", "monday", df$tweet)
  df$tweet <- gsub("\\btue\\b", "tuesday", df$tweet)
  df$tweet <- gsub("\\bwed\\b", "wednesday", df$tweet)
  df$tweet <- gsub("\\bthur\\b", "thursday", df$tweet)
  df$tweet <- gsub("\\bsat\\b", "saturday", df$tweet)
  df$tweet <- gsub("\\bsun\\b", "sunday", df$tweet)
  df$tweet <- gsub("\\bjan\\b", "january", df$tweet)
  df$tweet <- gsub("\\bfeb\\b", "febuary", df$tweet)
  df$tweet <- gsub("\\bmar\\b", "march", df$tweet)
  df$tweet <- gsub("\\bapri\\b", "april", df$tweet)
  df$tweet <- gsub("\\bjun\\b", "june", df$tweet)
  df$tweet <- gsub("\\baug\\b", "august", df$tweet)
  df$tweet <- gsub("\\bsep\\b", "september", df$tweet)
  df$tweet <- gsub("\\boct\\b", "october", df$tweet)
  df$tweet <- gsub("\\bnov\\b", "november", df$tweet)
  df$tweet <- gsub("\\bdec\\b", "december", df$tweet)
  df$tweet <- gsub("\\bpwr\\b", "power", df$tweet)
  
  
  
  
  #df$tweet <- gsub('[]#%$^*\\~{\}[&+=@"`|<>_-]+', "",df$tweet)
  #df$tweet <- gsub("[!?,.]+", ".", df$tweet)
  
  
  #get rid of unnecessary spaces
  df$tweet <- str_replace_all(df$tweet," "," ")
  df$tweet <- stripWhitespace(df$tweet)
  

  
  df$tweet <- gsub("[¤º-»«Ã¢â¬Å¥¡Â¿°£·©Ë¦¼¹¸±???ð\u201E\u201F\u0097\u0083\u0082\u0080\u0081\u0090\u0095\u009f\u0098\u008d\u008b\u0089\u0087\u008a¦??.]+", " ", df$tweet)
  df$tweet <- gsub("[\002\020\023\177\003]", "",df$tweet)
  # these to be apostrophes
  df$tweet <- gsub("[\u0092]","'",df$tweet)
  df$tweet <- gsub("[\u2019]","'",df$tweet)
  df$tweet <- gsub("[\u00b4]","'",df$tweet)
  # replace the graph characters
  df$tweet <- str_replace_all(df$tweet,"[^[:graph:]]", " ")
  #convert to lowercase
  df$tweet <- tolower(df$tweet)
  
  df$tweet <- gsub("u002c", ",", df$tweet)
  

  #Removing Not Available tweets
  
  # GET ALL THE INDEX WITH NOT AVAILABLE EXACTLY !
  
  lapply(lapply(df, function(x) x == "not available"), table)
  index <- df[,2] != "not available"
  df <- df[index,]
  
  #df$tweet <- gsub('http.*', "", df$tweet)
  df$tweet <- gsub("(f|ht)tp(s?):[/]?/(.*)[.][a-z]?", "", df$tweet)
  df$tweet <- gsub('i \'m', "i am", df$tweet)
  df$tweet <- gsub('they \'re', "they are", df$tweet)
  df$tweet <- gsub('they \'ve', "they have", df$tweet)
  df$tweet <- gsub('it \'s', "it is", df$tweet)
  df$tweet <- gsub('\\bdont\\b', 'don\'t', df$tweet)
  df$tweet <- gsub('\\bdnt\\b', 'don\'t', df$tweet)
  
  df$tweet <- gsub(':)', ' Happy ', df$tweet)
  df$tweet <- gsub(':D', ' happy ', df$tweet)
  df$tweet <- gsub('lol', "happy", df$tweet)
  
  df$tweet <- gsub("\'", '', df$tweet)
  
  df$tweet <- bracketX(df$tweet)
  
  df$tweet <- replace_abbreviation(df$tweet)
  
  df$tweet <- replace_contraction(df$tweet)
  
  df$tweet <- removePunctuation(df$tweet)
  
  return(df)
}
############################# END ########################


get_bigram_score <- function(df){
  
 df<- df %>% mutate(linenumber = row_number())
  
  tweet_bigram <- df %>%
    unnest_tokens(bigram, tweet, token = "ngrams", n = 2)
  
  tweet_bigram %>%
    count(bigram, sort = TRUE)
  
  
  bigrams_separated <- tweet_bigram %>%
    separate(bigram, c("word1", "word2"), sep = " ")
  
  bigrams_filtered <- bigrams_separated %>%
    filter(!word1 %in% stop_words$word) %>%
    filter(!word2 %in% stop_words$word)
  
  bigram_counts <- bigrams_filtered %>% 
    count(word1, word2, sort = TRUE)

  bigrams_separated %>%
    filter(word1 == "not") %>%
    count(word1, word2, sort = TRUE)
  
  AFINN <- get_sentiments("afinn")
  negation_words <- c("not", "no", "never", "without")
  
  not_words <- bigrams_separated %>%
    filter(word1 == "not" | word1 == "no"| word1 == "never"| word1 == "without") %>%
    inner_join(AFINN, by = c(word2 = "word")) %>%
    count(word2, score,linenumber, sort = TRUE) %>%
    ungroup()
  
  final_score <- not_words %>% mutate(final_score = -score * n)
  
  test_df <- df %>% left_join(final_score, by = c(linenumber = "linenumber"))
  
  df <- test_df %>% group_by(linenumber) %>% summarise(sentiment = unique(sentiment), 
                                                            tweet = unique(tweet), 
                                                            bigram_score = sum(final_score))
  df[is.na(df)] <- 0
  
  not_words %>%
    mutate(contribution = n * score) %>%
    arrange(desc(abs(contribution))) %>%
    head(20) %>%
    mutate(word2 = reorder(word2, contribution)) %>%
    ggplot(aes(word2, n * score, fill = n * score > 0)) +
    geom_col(show.legend = FALSE) +
    xlab("Words preceded by \"not\"") +
    ylab("Sentiment score * number of occurrences") +
    coord_flip()
  
  return(df)
  
  }



################## STEMMING FOR WORDS ####################

perform_stemming <- function(df){
  
  
  # stopwords_regex = paste(stopwords('en'), collapse = '\\b|\\b')
  # stopwords_regex = paste0('\\b', stopwords_regex, '\\b')
  # df$tweet = stringr::str_replace_all(df$tweet, stopwords_regex, '')
  # 
  tidy_tweet <- df %>% 
    mutate(linenumber = row_number()) %>%
    unnest_tokens(word, tweet) %>%
    ungroup()
  
 
  
  
  #write the word list to a file
  write.table(tidy_tweet$word, "c:/TreeTagger/word_list_tweet.txt", sep = "\n", row.names = F, col.names = F, quote = FALSE)
  
  #Run treetagger lemmatization on the file
  status <- system("C:/TreeTagger/bin/tag-english C:/TreeTagger/word_list_tweet.txt", ignore.stderr = F, intern = TRUE )
  
  #Read the output back into the environment
  lemmatized_words <- (str_split(status, "\t"))
  tidy_tweet_lemmatized <- setNames(do.call(rbind.data.frame, lemmatized_words), c("Word", "POS TAG", "lemma"))
  tidy_tweet_lemmatized$lemma <- as.character(tidy_tweet_lemmatized$lemma)
  #tidy_tweet_lemmatized <- (tidy_tweet_lemmatized[,3, drop = F])
  n <- nrow(tidy_tweet_lemmatized)
  tidy_tweet_lemmatized <- tidy_tweet_lemmatized[1:(n-3), , drop = F]
  
  tidy_tweet_lemmatized <- cbind(tidy_tweet, tidy_tweet_lemmatized)
  test <- tidy_tweet_lemmatized %>% group_by(linenumber, `POS TAG`) %>% summarise(count = n())
  join_this <- spread(test, `POS TAG`, count)
  finally <- inner_join(tidy_tweet_lemmatized, join_this, by = (linenumber = "linenumber"))
  finally_new <- finally[,c("sentiment", "bigram_score", "linenumber", "word", "lemma", "DT", "VB", "JJ", "JJR", "JJS", "VBD","VBG", "VBN", "VBZ", "VBP",  "VH", "VHD", "VHG", "VHN", "VHZ", "VHP", "VV", "VVN", "VVG", "VVD", "VVP", "VVZ", "RBR")]
  finally_new[is.na(finally_new)] <- 0
  finally_new$verb_count <- finally_new$VBD + finally_new$VBG + finally_new$VB + finally_new$VBN + finally_new$VBP + finally_new$VBZ + finally_new$VH + finally_new$VHD + finally_new$VHG + finally_new$VHN + finally_new$VHP + finally_new$VHZ + finally_new$VV + finally_new$VVD + finally_new$VVG + finally_new$VVN + finally_new$VVP + finally_new$VVZ
  
  finally_new <- finally_new[,c("sentiment", "bigram_score", "linenumber", "word", "lemma", "DT", "JJR", "JJS", "verb_count", "RBR")]
  finally[is.na(finally)] <- 0
  finally$adj_count <- finally$JJ + finally$JJS + finally$JJR  
  
  finally_new$adj_count <- finally$adj_count
  
  finally$adverb_count <- finally$RB + finally$RBS + finally$RBR 
  finally_new$adverb_count <- finally$adverb_count
  
  finally_new <- finally_new[, c("sentiment","bigram_score", "linenumber", "word", "lemma", "DT", "verb_count", "adj_count", "adverb_count")]
  
  return(finally_new) 
  
}

################################# END ############################


get_sentiments_bing <- function(df){
  
  bing <- get_sentiments("bing")
  
  df <- rename(df, op_sentiment = sentiment)
  sentiment_tidy_tweet <- df %>% left_join(get_sentiments("nrc"))

  
  colnames(sentiment_tidy_tweet ) <- c("op_sentiment", "bigram_score", "linenumber", "word", "lemma", "DT", "verb_count", "adj_count","adverb_count", "ncr_sentiment")
  sentiment_tidy_tweet <- sentiment_tidy_tweet %>% left_join(bing, by = (nct_sentiment = "word"))
  
  
  sentiment_tidy_tweet <- sentiment_tidy_tweet %>% mutate(score = ifelse(sentiment == "negative", -1, 1))
  
  sentiment_tidy_tweet <- sentiment_tidy_tweet %>% mutate(score = ifelse(is.na(score), 0, score))
  
  sentiment_tidy_tweet <- sentiment_tidy_tweet %>% group_by(linenumber) %>% 
    summarise(op_sentiment = unique(op_sentiment), bigram_score = sum(bigram_score) /n(), verb_count = sum(verb_count) / n(), 
              adverb_count = sum(adverb_count) / n(), adj_count = sum(adj_count) / n(), 
              score = sum(score))
  
  return(sentiment_tidy_tweet)
  
}



######################### POS TAGGING ###########################

# perform_POS_tagging <- function(x){
#   
#   
#   
#   write.table(df$tweet, "C:/ark-tweet-nlp-0.3.2/df_fixed_1.txt", sep = "\n", row.names = F, col.names = F, quote = FALSE)
#   
#   
#   javaPosTagging <- system("java -Xmx500m -jar C:\\ark-tweet-nlp-0.3.2\\ark-tweet-nlp-0.3.2.jar --output-format conll  C:\\ark-tweet-nlp-0.3.2\\df_fixed_1.txt", ignore.stderr = F, intern = TRUE )
#   
#   javaPosTagging
#   
#   javaPosTagging <- (str_split(javaPosTagging, "Detected text input format"))
#   javaPosTagging <- (str_split(javaPosTagging, "\t"))
#   
#   pos_tagged_df <- setNames(do.call(rbind.data.frame, javaPosTagging), c("Word", "POS TAG", "Confidence"))
#   
#   pos_tagged_df <- pos_tagged_df[-1,]
#   
#   
#   # 
#   # # Add POS tag to the feature set
#   # 
#   # # create a tagging DF
#   # final_tagging <- array(dim = length(df))
#   # temp_tagging = ""
#   # k = 1
#   # for(i in 1:nrow(pos_tagged_df)){
#   #   
#   #   if(pos_tagged_df[i,1] != ""){
#   #     temp_tagging = paste(temp_tagging , pos_tagged_df[i,2], sep =  ";")
#   #   }else{
#   #     final_tagging[k] = temp_tagging
#   #     k = k + 1
#   #     temp_tagging = ""
#   #   }
#   #   
#   # }
#   # 
#   # # Count each tag and score them.
#   # 
#   # final_tagging <- as.data.frame(final_tagging)
#   # 
#   # 
#   # df_pos_tagged_data <- cbind(df, final_tagging)
#   # colnames(df_pos_tagged_data) <- c("sentiment", "tweet", "pos_tagging")
#   # 
#   # tagging_df <- data.frame(verb.count = as.numeric(), adverb.count = as.numeric(), 
#   #                          noun.count = as.numeric(), adjective.count = as.numeric(), 
#   #                          emoticon.count = as.numeric())
#   # 
#   # 
#   # 
#   # for(i in 1:nrow(df_pos_tagged_data)){
#   #   verb_count = 0
#   #   adverb_count = 0
#   #   noun_count = 0
#   #   adjective_count = 0
#   #   emoticon_count = 0
#   #   
#   #   row_tag <- str_split(df_pos_tagged_data[i,3], pattern = ";")
#   #   
#   #   for(j in 1:length(row_tag[[1]])){
#   #     
#   #     if (str_detect(row_tag[[1]][j], "V") ){
#   #       verb_count <- verb_count + 1
#   #     }
#   #     if (str_detect(row_tag[[1]][j], "R")){
#   #       adverb_count <- adverb_count + 1
#   #     }
#   #     if (str_detect(row_tag[[1]][j], "A")){
#   #       adjective_count <- adjective_count + 1
#   #     }
#   #     if (str_detect(row_tag[[1]][j], "N") || str_detect(row_tag[[1]][j], "S") || str_detect(row_tag[[1]][j], "Z")){
#   #       noun_count <- noun_count + 1
#   #     }
#   #     
#   #     if( str_detect(row_tag[[1]][j], "E")){
#   #       emoticon_count <- emoticon_count + 1
#   #     }
#   #     
#   #   }
#   #   temp_df <- data.frame(verb_count, adverb_count, adjective_count, noun_count, emoticon_count)
#   #   tagging_df <- rbind(tagging_df, temp_df)
#   #   
#   #   
#   # }
#   # 
#   # 
#   # df <- cbind(df, tagging_df)
#   # 
#   # df$sentiment <- as.factor(df$sentiment)
#   # 
#   
#   return(df)
# }
############################# END ###############################################


naive_bayes <- function(df){

  df$op_sentiment <- as.factor(df$op_sentiment)
  smp_size <- floor(0.60 * nrow(df))
  
  ## set the seed to make your partition reproductible
  set.seed(123)
  train_ind <- sample(seq_len(nrow(df)), size = smp_size)
  
  train <- df[train_ind, ]
  test <- df[-train_ind, ]
  
  
  
  model <- naiveBayes(op_sentiment ~ score + verb_count + adverb_count + adj_count + bigram_score , data = train)
  summary(model)
  print(model)
  
  
  preds <- predict(model, newdata = test)
  
  conf_matrix <- table(preds, test$op_sentiment)
  conf_matrix
  return(conf_matrix)
}


svm_model <- function(df){
  
  smp_size <- floor(0.60 * nrow(df))
  
  ## set the seed to make your partition reproductible
  set.seed(123)
  train_ind <- sample(seq_len(nrow(df)), size = smp_size)
  
  
  train <- df[train_ind, ]
  test <- df[-train_ind, ]
  
  
  
  model <- svm(op_sentiment ~ score + verb_count + adverb_count + adj_count + bigram_score, type = "C-classification", data = train)
  summary(model)
  print(model)
  
  
  preds <- predict(model, newdata = test)
  
  conf_matrix <- table(preds, test$op_sentiment)
  conf_matrix
  return(conf_matrix)
}






load_libraries()
df <- load_data()
df <- clean_data(df)
df <- get_bigram_score(df)

#df <- perform_POS_tagging(df)

df <- perform_stemming(df)

new_df <- df

df <- get_sentiments_bing(df)


conf_matrix <- naive_bayes(df)
conf_matrix


conf_matrix <- svm_model(df)
conf_matrix



















